package com.bargin.alexey.fuelbuddy.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

public abstract class BaseFragment extends Fragment {
    public static final int COUNT_OF_LIST_ITEMS = 4;
    private int layout;

    protected abstract int getFragmentLayout();

    protected abstract void initFragmentsViews(View rootView);

    protected abstract void attachFragmentsViews(View rootView);

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        layout = getFragmentLayout();
        View rootView = inflater.inflate(layout, container, false);
        attachFragmentsViews(rootView);
        initFragmentsViews(rootView);
        return rootView;
    }
}
