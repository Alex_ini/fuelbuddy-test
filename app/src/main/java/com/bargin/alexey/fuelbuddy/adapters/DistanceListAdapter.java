package com.bargin.alexey.fuelbuddy.adapters;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewTreeObserver;
import android.widget.ImageView;
import android.widget.TextView;

import com.bargin.alexey.fuelbuddy.R;
import com.bargin.alexey.fuelbuddy.interfaces.OnFuelingClick;
import com.bargin.alexey.fuelbuddy.models.Fueling;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class DistanceListAdapter extends BaseRecyclerViewAdapter {
    private OnFuelingClick listener;

    public DistanceListAdapter(Activity activity, List list, OnFuelingClick listener) {
        super(activity, list);
        this.listener = listener;
    }

    @Override
    protected int getRowLayoutId() {
        return R.layout.item_recycler_view;
    }

    @Override
    protected BaseViewHolder createViewHolder(View view) {
        Collections.sort(list, new Comparator<Fueling>() {
            public int compare(Fueling obj1, Fueling obj2) {
                return Integer.valueOf((int) obj2.getDistance()).compareTo((int) obj1.getDistance());
            }
        });
        return new DistanceListAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {
        if (list.size() > 0) {
            final Fueling item = (Fueling) list.get(position);
            if (item != null) {
                DistanceListAdapter.ViewHolder viewHolder = (DistanceListAdapter.ViewHolder) holder;
                viewHolder.bind((Fueling) list.get(position));
                viewHolder.itemView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        listener.onFuelingClick(item);
                    }
                });
            }
        }
    }

    class ViewHolder extends BaseRecyclerViewAdapter.BaseViewHolder {
        private TextView time;
        private TextView name;
        private TextView price;
        private TextView address;
        private TextView distance;
        private ImageView logo;
        private ImageView arrow;

        public ViewHolder(final View v) {
            super(v);
            time = (TextView) v.findViewById(R.id.time);
            name = (TextView) v.findViewById(R.id.name);
            price = (TextView) v.findViewById(R.id.price);
            address = (TextView) v.findViewById(R.id.address);
            distance = (TextView) v.findViewById(R.id.distance);
            logo = (ImageView) v.findViewById(R.id.logo);
            arrow = (ImageView) v.findViewById(R.id.arrow);
        }

        public void bind(Fueling fueling) {
            price.setText(fueling.getPrice() + " \u0584");
            time.setText(fueling.getTime());
            name.setText(fueling.getName());
            address.setText(fueling.getAddress());
            if (String.valueOf(fueling.getDistance()).length() > 3)
                distance.setText(String.valueOf(fueling.getDistance()).substring(0, 3) + ".." + activity.getString(R.string._km));
            else
                distance.setText(fueling.getDistance() + ".." + activity.getString(R.string._km));
            if (fueling.getName().equals(activity.getResources().getString(R.string.shell)))
                logo.setImageResource(R.drawable.shell_icon);
            else
                logo.setImageResource(R.drawable.gasprom_icon);
            name.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
                @Override
                public void onGlobalLayout() {
                    logo.getLayoutParams().height = name.getHeight();
                    arrow.getLayoutParams().height = name.getHeight();
                    logo.requestLayout();
                }
            });
        }
    }
}