package com.bargin.alexey.fuelbuddy.adapters;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

public abstract class BaseRecyclerViewAdapter<VH extends BaseRecyclerViewAdapter.BaseViewHolder> extends RecyclerView.Adapter<BaseRecyclerViewAdapter.BaseViewHolder> {
    protected List list;
    protected LayoutInflater inflater;
    protected Activity activity;
    protected RecyclerViewClickListener listener;

    public void setListener(RecyclerViewClickListener listener) {
        this.listener = listener;
    }

    public List getList() {
        return list;
    }

    public void setList(List list) {
        this.list = list;
        notifyDataSetChanged();
    }

    public BaseRecyclerViewAdapter(Activity activity, List list) {
        this.inflater = activity.getLayoutInflater();
        this.list = list;
        this.activity = activity;
    }

    public Object getItem(int index) {
        return list.get(index);
    }

    @Override
    public BaseViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = inflater.inflate(getRowLayoutId(), parent, false);
        setRowListener(view);
        return createViewHolder(view);
    }

    protected void setRowListener(View rowView) {
        rowView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (listener != null)
                    listener.onRecyclerViewClick(v);
            }
        });
    }

    protected abstract BaseViewHolder createViewHolder(View view);

    protected int getRowLayoutId() {
        return 0;
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class BaseViewHolder extends RecyclerView.ViewHolder {
        public BaseViewHolder(View view) {
            super(view);
        }
    }

    public interface RecyclerViewClickListener {
        void onRecyclerViewClick(View v);
    }

}