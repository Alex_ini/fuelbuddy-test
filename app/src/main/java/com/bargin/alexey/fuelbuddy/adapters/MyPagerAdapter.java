package com.bargin.alexey.fuelbuddy.adapters;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.bargin.alexey.fuelbuddy.fragments.DistanceFragment;
import com.bargin.alexey.fuelbuddy.fragments.PriceFragment;
import com.bargin.alexey.fuelbuddy.interfaces.OnFuelingClick;
import com.bargin.alexey.fuelbuddy.models.Fueling;

import java.util.List;

public class MyPagerAdapter extends FragmentPagerAdapter {
    private int NUM_ITEMS = 2;
    private static MyPagerAdapter myPagerAdapter;
    private List<Fueling> fueling;
    public static final int ITEM_DISTANCE_FRAGMENT = 0;
    public static final int ITEM_PRICE_FRAGMENT = 1;
    private OnFuelingClick listener;

    public static MyPagerAdapter newInstance(FragmentManager fragmentManager, List<Fueling> fueling, OnFuelingClick listener) {
        if (myPagerAdapter == null)
            myPagerAdapter = new MyPagerAdapter(fragmentManager, fueling, listener);
        return myPagerAdapter;
    }

    private MyPagerAdapter(FragmentManager fragmentManager, List<Fueling> fueling, OnFuelingClick listener) {
        super(fragmentManager);
        this.fueling = fueling;
        this.listener = listener;
    }

    @Override
    public int getCount() {
        return NUM_ITEMS;
    }

    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case ITEM_DISTANCE_FRAGMENT:
                return DistanceFragment.newInstance(fueling, listener);
            case ITEM_PRICE_FRAGMENT:
                return PriceFragment.newInstance(fueling, listener);
            default:
                return null;
        }
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return "Page " + position;
    }

}
