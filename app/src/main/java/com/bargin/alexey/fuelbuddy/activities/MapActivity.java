package com.bargin.alexey.fuelbuddy.activities;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.util.DisplayMetrics;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bargin.alexey.fuelbuddy.R;
import com.bargin.alexey.fuelbuddy.adapters.MyPagerAdapter;
import com.bargin.alexey.fuelbuddy.interfaces.OnFuelingClick;
import com.bargin.alexey.fuelbuddy.models.Fueling;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.ArrayList;
import java.util.List;

public class MapActivity extends AppCompatActivity implements OnMapReadyCallback, View.OnClickListener, OnFuelingClick {
    private static GoogleMap map;
    private SupportMapFragment mapView;
    private Marker marker;
    private final float mapZoom = 16;
    private List<Fueling> fuelingList;
    private ViewPager viewpager;
    private View priceTab;
    private View distanceTab;
    private LocationManager locationManager;
    private MyPagerAdapter adapter;
    private boolean isListWasHidden;
    private float hideY;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_map);
        setupFuelingList();
        ActionBar actionBar = getSupportActionBar();
        actionBar.hide();
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        mapView = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.fragment_map);
        mapView.getMapAsync(this);
        initBottomViewPager();
        priceTab = findViewById(R.id.price_tab);
        priceTab.setOnClickListener(this);
        distanceTab = findViewById(R.id.distance_tab);
        distanceTab.setOnClickListener(this);
        setBottomNavigationTabsSelected(viewpager.getCurrentItem());
        findViewById(R.id.edittext_search).setOnKeyListener(new View.OnKeyListener() {
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (event.getAction() == KeyEvent.ACTION_DOWN) {
                    if (keyCode == KeyEvent.KEYCODE_ENTER)
                        hideKeyboard();
                }
                return false;
            }
        });
        viewpager.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                mapView.getView().animate().translationY(-(viewpager.getHeight() / 2));
                viewpager.getViewTreeObserver().removeOnGlobalLayoutListener(this);
            }
        });
    }

    private Location getLocation() {
        Location location = new Location(LocationManager.PASSIVE_PROVIDER);
        try {
            if (locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER))
                location = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
            else if (locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER))
                location = locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
            else
                location = locationManager.getLastKnownLocation(LocationManager.PASSIVE_PROVIDER);
        } catch (SecurityException e) {
            location = locationManager.getLastKnownLocation(LocationManager.PASSIVE_PROVIDER);
        } finally {
            return location;
        }
    }

    private void setupFuelingList() {
        locationManager = (LocationManager) getApplicationContext().getSystemService(Context.LOCATION_SERVICE);
        Location currentLocation = getLocation();
        fuelingList = new ArrayList<>();
        fuelingList.add(new Fueling(this, R.string.shell, R.string.address_1, 35.5f, "час назад", 1056));
        fuelingList.add(new Fueling(this, R.string.gasprom, R.string.address_2, 35.5f, "2 часа назад", 825));
        fuelingList.add(new Fueling(this, R.string.gasprom, R.string.address_3, 35.5f, "3 часа назад", 970));
        fuelingList.add(new Fueling(this, R.string.gasprom, R.string.address_4, 35.5f, "3 часа назад", 515));
        for (Fueling fueling : fuelingList) {
            Location loc = new Location("");
            loc.setLatitude(fueling.getLatitude());
            loc.setLongitude(fueling.getLongitude());
            fueling.setDistance(currentLocation.distanceTo(loc) / 1000);
        }
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        map = googleMap;
        updateMap(fuelingList.get(0));
    }

    private void updateMap(final Fueling fueling) {
        if (marker != null)
            marker.remove();
        CameraUpdate cameraUpdate;
        LatLng location;
        double latitude = fueling.getLatitude();
        double longitude = fueling.getLongitude();
        location = new LatLng(latitude, longitude);
        View markerView = ((LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.custom_marker_layout, null);
        ((TextView) markerView.findViewById(R.id.marker_address)).setText(fueling.getAddress());
        ((TextView) markerView.findViewById(R.id.marker_price)).setText(fueling.getPrice() + " \u0584");
        if (fueling.getName().equals(getResources().getString(R.string.shell)))
            ((ImageView) markerView.findViewById(R.id.marker_logo)).setImageResource(R.drawable.shell_icon);
        else
            ((ImageView) markerView.findViewById(R.id.marker_logo)).setImageResource(R.drawable.gasprom_icon);
        MarkerOptions markerOptions = new MarkerOptions();
        markerOptions
                .icon(BitmapDescriptorFactory.fromBitmap(createDrawableFromView(this, markerView)))
                .anchor(0.2f, 0.5f).position(location);
        marker = map.addMarker(markerOptions);
        cameraUpdate = CameraUpdateFactory.newLatLngZoom(location, mapZoom);
        map.moveCamera(cameraUpdate);
    }

    public static Bitmap createDrawableFromView(Context context, View view) {
        DisplayMetrics displayMetrics = new DisplayMetrics();
        ((Activity) context).getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        view.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT));
        view.measure(displayMetrics.widthPixels, displayMetrics.heightPixels);
        view.layout(0, 0, displayMetrics.widthPixels, displayMetrics.heightPixels);
        view.buildDrawingCache();
        Bitmap bitmap = Bitmap.createBitmap(view.getMeasuredWidth(), view.getMeasuredHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(bitmap);
        view.draw(canvas);
        return bitmap;
    }

    private void initBottomViewPager() {
        viewpager = (ViewPager) findViewById(R.id.viewpager);
        viewpager.getLayoutParams().height = (int) (getWindowManager().getDefaultDisplay().getHeight() / 2.5);
        adapter = MyPagerAdapter.newInstance(getSupportFragmentManager(), fuelingList, this);
        viewpager.setAdapter(adapter);
        viewpager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                setBottomNavigationTabsSelected(position);
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
    }

    private void setBottomNavigationTabsSelected(int position) {
        switch (position) {
            case MyPagerAdapter.ITEM_DISTANCE_FRAGMENT:
                ((TextView) distanceTab).setTextColor(Color.WHITE);
                ((TextView) priceTab).setTextColor(getResources().getColor(R.color.gray_text));
                break;
            case MyPagerAdapter.ITEM_PRICE_FRAGMENT:
                ((TextView) priceTab).setTextColor(Color.WHITE);
                ((TextView) distanceTab).setTextColor(getResources().getColor(R.color.gray_text));
                break;
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.distance_tab:
                viewpager.setCurrentItem(MyPagerAdapter.ITEM_DISTANCE_FRAGMENT);
                break;
            case R.id.price_tab:
                viewpager.setCurrentItem(MyPagerAdapter.ITEM_PRICE_FRAGMENT);
                break;
            case R.id.marker_heart:
                showMessage("You like this location");
                break;
            case R.id.marker_arrow:
                showMessage("You try to navigate to this location");
                break;
            case R.id.profile:
                showMessage("Profile");
                break;
            case R.id.settings:
                showMessage("Settings");
                break;
            case R.id.ic_location:
                showMessage("Location button");
                break;
            case R.id.plus_button:
                showMessage("Plus button");
                break;
            case R.id.hide_button:
                final int DURATION = 500;
                float mapTranslation = 0;
                if (!isListWasHidden) {
                    isListWasHidden = true;
                    if (hideY == 0)
                        hideY = viewpager.getHeight() - viewpager.getHeight() / 4;
                } else {
                    mapTranslation = -viewpager.getHeight() / 2;
                    isListWasHidden = false;
                    hideY = 0;
                }
                viewpager.animate().setDuration(DURATION).translationY(hideY);
                findViewById(R.id.bottom_navigation_bar).animate().setDuration(DURATION).translationY(hideY);
                findViewById(R.id.hide_button_container).animate().setDuration(DURATION).translationY(hideY);
                mapView.getView().animate().translationY(mapTranslation).setDuration(DURATION).start();
                break;
        }
    }

    @Override
    public void onFuelingClick(Fueling fueling) {
        updateMap(fueling);
    }

    private void showMessage(String message) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }

    private void hideKeyboard() {
        InputMethodManager imm = (InputMethodManager) getSystemService(Activity.INPUT_METHOD_SERVICE);
        View view = getCurrentFocus();
        if (view == null) {
            view = new View(this);
        }
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }
}
