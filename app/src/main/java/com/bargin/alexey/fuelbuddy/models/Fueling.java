package com.bargin.alexey.fuelbuddy.models;

import android.content.Context;
import android.location.Address;
import android.location.Geocoder;

import java.io.IOException;
import java.util.List;
import java.util.Locale;

public class Fueling {
    private Context context;
    private double longitude;
    private double latitude;
    private String name;
    private String address;
    private float distance;
    private float price;
    private String time;
    private int moneyCount;

    public Fueling(Context context, int nameId, int addressId, float price, String time, int moneyCount) {
        this.context = context;
        this.name = context.getString(nameId);
        this.address = context.getString(addressId);
        this.price = price;
        this.time = time;
        this.moneyCount = moneyCount;
        setLatLng(address);
    }

    public int getMoneyCount() {
        return moneyCount;
    }

    public String getName() {
        return name;
    }

    public String getAddress() {
        return address;
    }

    public double getLongitude() {
        return longitude;
    }

    public double getLatitude() {
        return latitude;
    }

    public float getDistance() {
        return distance;
    }

    public void setDistance(float distance) {
        this.distance = distance;
    }

    public float getPrice() {
        return price;
    }

    public String getTime() {
        return time;
    }

    private void setLatLng(String address) {
        Geocoder geocoder = new Geocoder(context, Locale.getDefault());
        List<Address> addresses = null;
        try {
            addresses = geocoder.getFromLocationName(address, 1);
        } catch (IOException e) {
            e.printStackTrace();
        }
        if (addresses.size() > 0) {
            longitude = addresses.get(0).getLongitude();
            latitude = addresses.get(0).getLatitude();
        }
    }

}
