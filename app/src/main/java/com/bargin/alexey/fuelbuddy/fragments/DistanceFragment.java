package com.bargin.alexey.fuelbuddy.fragments;

import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.bargin.alexey.fuelbuddy.R;
import com.bargin.alexey.fuelbuddy.adapters.DistanceListAdapter;
import com.bargin.alexey.fuelbuddy.interfaces.OnFuelingClick;
import com.bargin.alexey.fuelbuddy.models.Fueling;

import java.util.List;

public class DistanceFragment extends BaseFragment {
    private static DistanceFragment distanceFragment;
    private List<Fueling> fueling;
    private RecyclerView recyclerView;
    private GridLayoutManager verticalGridLayoutManager;
    private static RecyclerView.Adapter adapter;
    private OnFuelingClick listener;

    public static DistanceFragment newInstance(List<Fueling> fueling, OnFuelingClick listener) {
        if (distanceFragment == null)
            distanceFragment = new DistanceFragment();
        distanceFragment.setFueling(fueling);
        distanceFragment.setListener(listener);
        return distanceFragment;
    }

    private void setListener(OnFuelingClick listener) {
        this.listener = listener;
    }

    private void setFueling(List<Fueling> fueling) {
        this.fueling = fueling;
    }

    @Override
    protected int getFragmentLayout() {
        return R.layout.fragment_template;
    }

    @Override
    protected void initFragmentsViews(View rootView) {
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(verticalGridLayoutManager);
        recyclerView.setAdapter(adapter);
    }

    @Override
    protected void attachFragmentsViews(View rootView) {
        recyclerView = (RecyclerView) rootView.findViewById(R.id.recyclerView);
        verticalGridLayoutManager = new GridLayoutManager(getContext(), BaseFragment.COUNT_OF_LIST_ITEMS, LinearLayoutManager.HORIZONTAL, true);
        adapter = new DistanceListAdapter(getActivity(), fueling, listener);
    }
}
