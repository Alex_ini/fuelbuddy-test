package com.bargin.alexey.fuelbuddy.fragments;

import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.bargin.alexey.fuelbuddy.R;
import com.bargin.alexey.fuelbuddy.adapters.PriceListAdapter;
import com.bargin.alexey.fuelbuddy.interfaces.OnFuelingClick;
import com.bargin.alexey.fuelbuddy.models.Fueling;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class PriceFragment extends BaseFragment {
    private static PriceFragment priceFragment;
    private List<Fueling> fueling;
    private RecyclerView recyclerView;
    private GridLayoutManager verticalGridLayoutManager;
    private static RecyclerView.Adapter adapter;
    private OnFuelingClick listener;

    public static PriceFragment newInstance(List<Fueling> fueling, OnFuelingClick listener) {
        if (priceFragment == null)
            priceFragment = new PriceFragment();
        priceFragment.setFueling(fueling);
        priceFragment.setListener(listener);
        return priceFragment;
    }

    private void setListener(OnFuelingClick listener) {
        this.listener = listener;
    }

    private void setFueling(List<Fueling> fueling) {
        this.fueling = fueling;
    }

    @Override
    protected int getFragmentLayout() {
        return R.layout.fragment_template;
    }

    @Override
    protected void initFragmentsViews(View rootView) {
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(verticalGridLayoutManager);
        recyclerView.setAdapter(adapter);
    }

    @Override
    protected void attachFragmentsViews(View rootView) {
        recyclerView = (RecyclerView) rootView.findViewById(R.id.recyclerView);
        verticalGridLayoutManager = new GridLayoutManager(getContext(), BaseFragment.COUNT_OF_LIST_ITEMS, LinearLayoutManager.HORIZONTAL, true);
        Collections.sort(fueling, new Comparator<Fueling>() {
            public int compare(Fueling obj1, Fueling obj2) {
                return Integer.valueOf(obj2.getMoneyCount()).compareTo(obj1.getMoneyCount());
            }
        });
        adapter = new PriceListAdapter(getActivity(), fueling, listener);
    }

}
