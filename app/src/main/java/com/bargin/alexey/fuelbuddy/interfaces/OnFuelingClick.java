package com.bargin.alexey.fuelbuddy.interfaces;

import com.bargin.alexey.fuelbuddy.models.Fueling;

public interface OnFuelingClick {
    void onFuelingClick(Fueling fueling);
}
